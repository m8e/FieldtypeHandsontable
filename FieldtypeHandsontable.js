/**
 * trigger pw inputfield toggles and tab changes
 */
$(document).on('opened wiretabclick', function(e) {
    $hot = $(e.target);

    // redraw all handsontables inside the toggled field
    $.each($hot.find('.handsontable'), function() {
        var hot = $(this).handsontable('getInstance');
        if(hot) hot.render();
    });
});
