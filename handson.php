<?php namespace ProcessWire;
class handson {

  /**
   * return all data
   */
  public function getData() {
    return $this->data;
  }

  /**
   * return all rows of the table
   * same as getData()
   */
  public function getRows() {
    return $this->data;
  }

  /**
   * return requested row of the table
   */
  public function getRow($row) {
    $row = $this->getRowIndex($row);
    $rows = count($this->data);
    
    if($row > $rows-1) {
      throw new WireException("$row not possible - this table has only $rows rows");
      return false;
    }
    return $this->data[$row];
  }

  /**
   * return all columns of the table
   */
  public function getCols() {
    $cols = [];
    foreach($this->getRow(0) as $k => $v) $cols[] = $this->getCol($k);
    return $cols;
  }

  /**
   * return requested column of the table
   */
  public function getCol($col) {
    $col = $this->getColIndex($col);
    $cols = count($this->getRow(0));
    $data = [];
    
    if($col > $cols-1) {
      throw new WireException("$col not possible - this table has only $cols columns");
      return false;
    }

    foreach($this->getRows() as $row) $data[] = $row[$col];
    return $data;
  }

  /**
   * return number of filled cells in this table
   */
  public function filledInTable() {
    $filled = 0;
    foreach($this->getRows() as $index => $row) $filled += $this->filledInRow($index);
    return $filled;
  }

  /**
   * return number of filled cells in this column
   */
  public function filledInCol($col) {
    if(gettype($col) == 'string') $col = $this->getColIndex($col);
    $filled = 0;
    foreach($this->getCol($col) as $cell) if($cell) $filled++;
    return $filled;
  }

  /**
   * return number of filled cells in this row
   */
  public function filledInRow($row) {
    if(gettype($row) == 'string') $row = $this->getRowIndex($row);
    $filled = 0;
    foreach($this->getRow($row) as $cell) if($cell) $filled++;
    return $filled;
  }

  /**
   * return row index from rowname
   */
  public function getRowIndex($rowname) {
    if(gettype($rowname) == 'integer') return $rowname;
    return array_search($rowname, $this->rowHeaders);
  }

  /**
   * return col index from colname
   */
  public function getColIndex($colname) {
    if(gettype($colname) == 'integer') return $colname;
    return array_search($colname, $this->colHeaders);
  }

}